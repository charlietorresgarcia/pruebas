#ifndef RESISTENCIAS_H
#define RESISTENCIAS_H

#include <iostream>
#include <vector>
using namespace std;

class Resistencias {
private:
    vector<double> resistencias;

public:
    void ingresarResistencias();
    double equivalenteSerie();
    double equivalenteParalelo();
    void mostrarResultado(const string& tipo_calculo, double resultado);
};

#endif // RESISTENCIAS_H

