#include "Resistencias.h"
using namespace std;

void Resistencias::ingresarResistencias() {
    cout << "Ingrese los valores de las resistencias. Ingrese un valor negativo para terminar." << endl;
    double valor;
    while (true) {
        cin >> valor;
        if (valor < 0) break;
        resistencias.push_back(valor);
    }
}

double Resistencias::equivalenteSerie() {
    double resistencia_total = 0;
    for (size_t i = 0; i < resistencias.size(); ++i) {
        resistencia_total += resistencias[i];
    }
    return resistencia_total;
}

double Resistencias::equivalenteParalelo() {
    double resistencia_total = 0;
    for (size_t i = 0; i < resistencias.size(); ++i) {
        resistencia_total += 1 / resistencias[i];
    }
    return 1 / resistencia_total;
}

void Resistencias::mostrarResultado(const string& tipo_calculo, double resultado) {
    cout << "\nResistencias ingresadas: ";
    for (size_t i = 0; i < resistencias.size(); ++i) {
        cout << resistencias[i] << " ";
    }
    cout << "\nResistencia equivalente en " << tipo_calculo << ": " << resultado << " ohms" << endl;
}

