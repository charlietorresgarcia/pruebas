#include "Resistencias.h"
using namespace std;

int main() {
    Resistencias calculadora;

    cout << "�Calcular en serie o en paralelo? ";
    string tipo_calculo;
    cin >> tipo_calculo;

    calculadora.ingresarResistencias();

    double resultado;
    if (tipo_calculo == "serie") {
        resultado = calculadora.equivalenteSerie();
    } else if (tipo_calculo == "paralelo") {
        resultado = calculadora.equivalenteParalelo();
    } else {
        cout << "Opci�n no v�lida." << endl;
        return 1;
    }

    calculadora.mostrarResultado(tipo_calculo, resultado);

    return 0;
}

